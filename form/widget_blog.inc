<?php
/**
 * @file
 * Code for blog widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_blog'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Blog widget'),
  '#description'    => t('Display a blog feed. The blog feed is populated with the "uw blog" content type.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('blog-widget')),
);

$form['conference_homepage']['conference_blog']['conference_blog_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_blog_title', ''),
  '#description'    => t('Optionally enter a title. The title appears at the top of the widget.'),
);

$conference_blog_intro = variable_get('conference_blog_intro', '');
if (is_array($conference_blog_intro)) {
  $conference_blog_intro = $conference_blog_intro['value'];
}

$term   = variable_get('conference_blog_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_blog']['conference_blog_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_blog_intro,
  '#description'    => t('Optionally enter an introduction. The introduction appears at the top of the widget, underneath the title.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-blog-intro-format { display: none; }</style>',
);

$form['conference_homepage']['conference_blog']['conference_blog_featured'] = array(
  '#type'           => 'checkbox',
  '#title'          => t('Highlight Featured'),
  '#default_value'  => variable_get('conference_blog_featured', FALSE),
  '#description'    => t('Optionally highlight a featured post. It will appear in a call-out. The featured post is the most recent sticky post.'),
);

$form['conference_homepage']['conference_blog']['conference_blog_button_label'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Button label'),
  '#default_value'  => variable_get('conference_blog_button_label', ''),
  '#description'    => t('Optionally enter a button label. This will create a button with this label that links to the blog page.'),
);
