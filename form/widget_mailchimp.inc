<?php
/**
 * @file
 * Code for mailchimp widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_mailchimp_block'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('MailChimp widget'),
  '#description'    => t('Display a MailChimp signup form. This widget uses the default Mailchimp settings for this site entered in the "conference settings" form.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('mailchimp-widget')),
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_mailchimp_block_title', ''),
  '#description'    => t('Optionally enter a title. The title will appear at the top of the widget.'),
);

$conference_mailchimp_block_body = variable_get('conference_mailchimp_block_body');
if (is_array($conference_mailchimp_block_body)) {
  $conference_mailchimp_block_body = $conference_mailchimp_block_body['value'];
}

$term   = variable_get('conference_mailchimp_block_body');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_body'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Body'),
  '#default_value'  => $conference_mailchimp_block_body,
  '#description'    => t('Optionally enter the body. The body will appear at the top of the widget, underneath the title.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-mailchimp-block-body-format { display: none; }</style>',
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_action'] = array(
  '#type'             => 'textfield',
  '#title'            => t('MailChimp action'),
  '#default_value'    => variable_get('conference_mailchimp_block_action', ''),
  '#description'      => t('Form action. This should be a URL.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_user'] = array(
  '#type'             => 'textfield',
  '#title'            => t('User'),
  '#default_value'    => variable_get('conference_mailchimp_block_user', ''),
  '#description'      => t('User/account ID.'),
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_list'] = array(
  '#type'             => 'textfield',
  '#title'            => t('List'),
  '#default_value'    => variable_get('conference_mailchimp_block_list', ''),
  '#description'      => t('List ID.'),
);

$form['conference_homepage']['conference_mailchimp_block']['conference_mailchimp_block_submit_text'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Submit button text'),
  '#default_value'  => variable_get('conference_mailchimp_block_submit_text', ''),
  '#description'    => t('Label text on the submit button. E.g. "Submit".'),
  '#maxlength'      => 12,
);
