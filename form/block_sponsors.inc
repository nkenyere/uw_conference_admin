<?php
/**
 * @file
 * Code for sponsors carousel in UW Conference Admin form.
 */

$form['conference']['conference_sponsors_carousel'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Sponsors Carousel'),
  '#description'    => t('Display sponsors in a carousel above the footer on all pages except the sponsors page.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('conference-sponsors-carousel')),
);

$form['conference']['conference_sponsors_carousel']['conference_sponsors_carousel_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_sponsors_carousel_title', ''),
  '#description'    => t('Optionally enter a title. The title appears at the top of the widget.'),
);


$conference_sponsors_carousel_intro = variable_get('conference_sponsors_carousel_intro', '');
if (is_array($conference_sponsors_carousel_intro)) {
  $conference_sponsors_carousel_intro = $conference_sponsors_carousel_intro['value'];
}

$format = 'uw_tf_basic';

$form['conference']['conference_sponsors_carousel']['conference_sponsors_carousel_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_sponsors_carousel_intro,
  '#description'    => t('Optionally enter an introduction. The introduction appears at the top of the widget, underneath the title.'),
  '#format'         => 'uw_tf_basic',
  'format'          => 'uw_tf_basic',
  '#suffix'         => '<style>#edit-conference-sponsors-carousel-intro-format { display: none; }</style>',
);

$form['conference']['conference_sponsors_carousel']['conference_sponsors_carousel_enabled'] = array(
  '#type'           => 'checkbox',
  '#title'          => t('Enabled'),
  '#default_value'  => variable_get('conference_sponsors_carousel_enabled', TRUE),
  '#description'    => t('Enable this feature?'),
);
