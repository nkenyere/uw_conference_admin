<?php
/**
 * @file
 * Code for speakers widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_speakers'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Speakers widget'),
  '#description'    => t('The speakers widget is used to display up to three speakers on the homepage. The speakers are populated with the "conference speaker" content type.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('speakers-widget')),
);

$form['conference_homepage']['conference_speakers']['conference_speakers_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_speakers_title', ''),
  '#description'    => t('Title to display above the speakers.'),
);

$conference_speakers_intro = variable_get('conference_speakers_intro');
if (is_array($conference_speakers_intro)) {
  $conference_speakers_intro = $conference_speakers_intro['value'];
}

$term   = variable_get('conference_speakers_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_speakers']['conference_speakers_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_speakers_intro,
  '#description'    => t('Introductory text to display above the speakers.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-speakers-intro-format { display: none; }</style>',
);
