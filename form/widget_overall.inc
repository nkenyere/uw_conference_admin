<?php
/**
 * @file
 * Code for overall widget settings in UW Conference Admin form.
 */

$form['conference_homepage']['conference_overall_theme'] = array(
  '#prefix'         => '<h2>Display and order widgets on the homepage</h2><p>Check enabled to show a widget on the homepage. Widgets are ordered by weight, from lowest to highest.</p><table>',
  '#suffix'         => '</table>',
);

$form['conference_homepage']['conference_overall_theme']['header'] = array(
  '#markup' => '<thead>
    <tr>
      <th>Widget</th>
      <th>Enabled</th>
      <th>Layout</th>
      <th>Background Colour</th>
      <th>Weight</th>
      <th>Additional Configuration</th>
    </tr>
  </thead>',
);

//Conference Agenda Widget

$form['conference_homepage']['conference_overall_theme']['conference_agenda'] = array(
  '#prefix'         => '<tr><td>' . t('Agenda widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_agenda_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_agenda_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_agenda_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_agenda_layout', 0),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_agenda_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_agenda_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_agenda_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_agenda_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_agenda_additional_settings'] = array(
  '#prefix'         => '<td><a href="#agenda-widget">Agenda widget settings</a>',
  '#suffix'         => '</td></tr>', // Close row
);

//End Agenda

//Conference Blog Widget//

$form['conference_homepage']['conference_overall_theme']['conference_blog_widget'] = array(
  '#prefix'         => '<tr><td>' . t('Blog widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_blog_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_blog_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_blog_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_blog_layout', 0),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_blog_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_blog_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_blog_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_blog_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_blog_additional_settings'] = array(
  '#prefix'         => '<td><a href="#blog-widget">Blog widget settings</a>',
  '#suffix'         => '</td></tr>', // Close row
);

//End Blog

//Conference Content Block Widget

$form['conference_homepage']['conference_overall_theme']['conference_content_block_widget'] = array(
  '#prefix'         => '<tr><td>' . t('Content block widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_content_block_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_content_block_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_content_block_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_content_block_layout', 0),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_content_block_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_content_block_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_content_block_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_content_block_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_content_block_additional_settings'] = array(
  '#prefix'         => '<td><a href="#content-block-widget">Content block widget</a>',
  '#suffix'         => '</td></tr>', // Close row
);

//End Content Block

//Conference Links Widget

$form['conference_homepage']['conference_overall_theme']['conference_links_widget'] = array(
  '#prefix'         => '<tr><td>' . t('Links widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_links_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_links_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_links_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_links_layout', 0),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_links_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_links_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_links_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_links_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_links_additional_settings'] = array(
  '#prefix'         => '<td><a href="#links-widget">Links widget settings</a>',
  '#suffix'         => '</td></tr>', // close row
);

//End Links

//Conference Location widget settings
$form['conference_homepage']['conference_overall_theme']['conference_location'] = array(
  '#prefix'         => '<tr><td>' . t('Location widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_location_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_location_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_location_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_location_layout', 0),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_location_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_location_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_location_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_location_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_location_additional_settings'] = array(
  '#prefix'         => '<td><a href="#location-widget">Location widget settings</a>',
  '#suffix'         => '</td></tr>', // Close row
);

//End Location

// Conference Mailchimp widget settings
$form['conference_homepage']['conference_overall_theme']['conference_mailchimp_block'] = array(
  '#prefix'         => '<tr><td>' . t('MailChimp widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_mailchimp_block_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_mailchimp_block_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_mailchimp_block_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_mailchimp_block_layout', ''),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_mailchimp_block_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_mailchimp_block_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_mailchimp_block_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_mailchimp_block_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_mailchimp_block_additional_settings'] = array(
  '#prefix'         => '<td><a href="#mailchimp-widget">MailChimp widget settings</a>',
  '#suffix'         => '</td></tr>', // Close row
);

// End Mailchimp

//Conference Speakers theme settings
$form['conference_homepage']['conference_overall_theme']['conference_speakers'] = array(
  '#prefix'         => '<tr><td>' . t('Speakers widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_speakers_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_speakers_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_speakers_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_speakers_layout', 0),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_speakers_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_speakers_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_speakers_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_speakers_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_speakers_additional_settings'] = array(
  '#prefix'         => '<td><a href="#speakers-widget">Speakers widget settings</a>',
  '#suffix'         => '</td></tr>', // Close row
);

//End Speakers

//Conference Tint Widget settings
$form['conference_homepage']['conference_overall_theme']['conference_tint'] = array(
  '#prefix'         => '<tr><td>' . t('Tint feed widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_tint_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_tint_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_tint_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_tint_layout', 0),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_tint_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_tint_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_tint_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_tint_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_tint_additional_settings'] = array(
  '#prefix'         => '<td><a href="#tint-feed-widget">Tint feed widget settings</a>',
  '#suffix'         => '</td></tr>', // Close row
);

//End Tint

//Conference Twitter Feed Widget
$form['conference_homepage']['conference_overall_theme']['conference_twitter_feed'] = array(
  '#prefix'         => '<tr><td>' . t('Twitter feed widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_twitter_feed_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_twitter_feed_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_twitter_feed_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_twitter_feed_layout', 0),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);
$form['conference_homepage']['conference_overall_theme']['conference_twitter_feed_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_twitter_feed_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_twitter_feed_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_twitter_feed_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_twitter_additional_settings'] = array(
  '#prefix'         => '<td><a href="#twitter-feed-widget">Twitter feed widget settings</a>',
  '#suffix'         => '</td></tr>', // Close row
);

// End Twitter Feed

//Conference Video Widget
$form['conference_homepage']['conference_overall_theme']['conference_videos'] = array(
  '#prefix'         => '<tr><td>' . t('Videos widget'),
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_videos_enabled'] = array(
  '#type'           => 'checkbox',
  '#default_value'  => variable_get('conference_videos_enabled', FALSE),
  '#prefix'         => '<td style="text-align:center">',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_videos_layout'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_videos_layout', 0),
  '#options'        => array(
    0 => t('Full Width'),
    1 => t('Half Width'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);
$form['conference_homepage']['conference_overall_theme']['conference_videos_background'] = array(
  '#type'           => 'select',
  '#default_value'  => variable_get('conference_videos_background', 0),
  '#options'        => array(
    0 => t('White'),
    1 => t('Gray'),
    2 => t('Dark Gray'),
  ),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_videos_weight'] = array(
  '#type'           => 'textfield',
  '#default_value'  => variable_get('conference_videos_weight', 0),
  '#prefix'         => '<td>',
  '#suffix'         => '</td>',
);

$form['conference_homepage']['conference_overall_theme']['conference_videos_additional_settings'] = array(
  '#prefix'         => '<td><a href="#videos-widget">Videos widget settings</a>',
  '#suffix'         => '</td></tr>', // Close row
);

//End Videos
