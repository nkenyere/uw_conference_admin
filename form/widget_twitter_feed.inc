<?php
/**
 * @file
 * Code for twitter feed widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_twitter_feed'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Twitter feed widget'),
  '#description'    => t('The Twitter feed to display tweets on the home page.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('twitter-feed-widget')),
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_feed_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_twitter_feed_title', ''),
  '#description'    => t('The title to appear above the tweets.'),
);

$conference_twitter_feed_intro = variable_get('conference_twitter_feed_intro', '');
if (is_array($conference_twitter_feed_intro)) {
  $conference_twitter_feed_intro = $conference_twitter_feed_intro['value'];
}

$term   = variable_get('conference_twitter_feed_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_feed_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_twitter_feed_intro,
  '#description'    => t('Introductory text that appears above the tweets.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-twitter-feed-intro-format { display: none; }</style>',
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_oauth_access_token'] = array(
  '#type'           => 'textfield',
  '#title'          => t('OAuth access token'),
  '#default_value'  => variable_get('conference_twitter_oauth_access_token', ''),
  '#description'    => t('OAuth access token for Twitter app.'),
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_oauth_access_token_secret'] = array(
  '#type'           => 'textfield',
  '#title'          => t('OAuth access token secret'),
  '#default_value'  => variable_get('conference_twitter_oauth_access_token_secret', ''),
  '#description'    => t('OAuth access token secret for Twitter app.'),
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_consumer_key'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Consumer key'),
  '#default_value'  => variable_get('conference_twitter_consumer_key', ''),
  '#description'    => t('Consumer key for Twitter app.'),
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_consumer_secret'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Consumer secret'),
  '#default_value'  => variable_get('conference_twitter_consumer_secret', ''),
  '#description'    => t('Consumer secret for Twitter app.'),
);

$form['conference_homepage']['conference_twitter_feed']['conference_twitter_query_string'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Query string'),
  '#default_value'  => variable_get('conference_twitter_query_string', ''),
  '#description'    => t('Search string for Twitter feed. E.g. #wins2015'),
);
