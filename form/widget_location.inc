<?php
/**
 * @file
 * Code for location widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_location'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Location widget'),
  '#description'    => t('Display a map on the homepage.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('location-widget')),
);

$form['conference_homepage']['conference_location']['conference_location_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_location_title', ''),
  '#description'    => t('Optionally enter a title. The title appears at the top of the widget.'),
);

$conference_location_intro = variable_get('conference_location_intro', '');
if (is_array($conference_location_intro)) {
  $conference_location_intro = $conference_location_intro['value'];
}

$term   = variable_get('conference_location_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_location']['conference_location_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_location_intro,
  '#description'    => t('Optionally enter an introduction. The introduction appears at the top of the widget, underneath the title.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-location-intro-format { display: none; }</style>',
);

$conference_location_address = variable_get('conference_location_address', '');
if (is_array($conference_location_address)) {
  $conference_location_address = $conference_location_address['value'];
}

$term   = variable_get('conference_location_address');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_location']['conference_location_address'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Address'),
  '#default_value'  => $conference_location_address,
  '#description'    => t('Enter an address. The address is not visible. It is a text alternative to the map for persons that require such.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-location-address-format { display: none; }</style>',
);

$form['conference_homepage']['conference_location']['conference_location_latitude'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Latitude'),
  '#default_value'  => variable_get('conference_location_latitude', ''),
  '#description'    => t('Enter the latitude. This is used to center the map and position the marker.'),
  '#element_validate' => array('_uw_conference_admin_validate_number'),
);

$form['conference_homepage']['conference_location']['conference_location_longitude'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Longitude'),
  '#default_value'  => variable_get('conference_location_longitude', ''),
  '#description'    => t('Enter the longitude. This is used to center the map and position the marker.'),
  '#element_validate' => array('_uw_conference_admin_validate_number'),
);

$form['conference_homepage']['conference_location']['conference_location_marker_icon'] = array(
  '#type'            => 'managed_file',
  '#title'           => t('Marker icon'),
  '#default_value'   => variable_get('conference_location_marker_icon', ''),
  '#description'     => t('Upload a custom marker. The marker should be 20x32 pixels and in PNG or SVG format. Leave blank to use the default marker.'),
  '#upload_location' => 'public://',
  '#upload_validators' => array(
    'file_validate_extensions'       => array('png svg'),
    'file_validate_size'             => array(5 * 1024 * 1024),
    'file_validate_image_resolution' => array('20x32', '20x32'),
  ),
);

$marker_fid = variable_get('conference_location_marker_icon');
if ($marker_fid) {
  $marker_file = file_load($marker_fid);
  if ($marker_file) {
    $marker_img  = '<img src="' . file_create_url($marker_file->uri) . '" alt="Marker Icon" style="border: 1px solid #ccc; max-width: 50%; padding: 5px;" />';
  }
  else {
    $marger_img = '';
  }
}
else {
  $marker_img = '';
}

$form['conference_homepage']['conference_location']['conference_location_marker_icon_preview'] = array(
  '#prefix' => '<p>',
  '#suffix' => $marker_img . '</p>',
);
