<?php
/**
 * @file
 * Code for contact information-related fields in UW Conference Admin form.
 */

$form['conference']['conference_contact_information'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Contact information'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
);

$form['conference']['conference_contact_information']['conference_contact_information_email'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Email'),
  '#default_value'  => variable_get('conference_contact_information_email', ''),
  '#description'    => t('Email address.'),
);

$form['conference']['conference_contact_information']['conference_social_handle'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Twitter handle'),
  '#default_value'  => variable_get('conference_social_handle', ''),
  '#description'    => t('Handle to use for Twitter campaigns.'),
);

$form['conference']['conference_contact_information']['conference_social_hashtag'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Twitter hashtag'),
  '#default_value'  => variable_get('conference_social_hashtag', ''),
  '#description'    => t('Hashtag to use for Twitter campaigns.'),
);

$form['conference']['conference_contact_information']['conference_social_twitter'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Twitter URL'),
  '#default_value'  => variable_get('conference_social_twitter', ''),
  '#description'    => t('Twitter account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_facebook'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Facebook URL'),
  '#default_value'  => variable_get('conference_social_facebook', ''),
  '#description'    => t('Facebook account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_instagram'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Instagram URL'),
  '#default_value'  => variable_get('conference_social_instagram', ''),
  '#description'    => t('Instagram account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_linkedin'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Linkedin URL'),
  '#default_value'  => variable_get('conference_social_linkedin', ''),
  '#description'    => t('Linkedin account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_storify'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Storify URL'),
  '#default_value'  => variable_get('conference_social_storify', ''),
  '#description'    => t('Storify account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_vimeo'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Vimeo URL'),
  '#default_value'  => variable_get('conference_social_vimeo', ''),
  '#description'    => t('Vimeo account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_social_youtube'] = array(
  '#type'           => 'textfield',
  '#title'          => t('YouTube URL'),
  '#default_value'  => variable_get('conference_social_youtube', ''),
  '#description'    => t('YouTube account link.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);
