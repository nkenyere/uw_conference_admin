<?php
/**
 * @file
 * Code for videos widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_videos'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Videos widget'),
  '#description'    => t('The videos widget is used to display up to four highlight videos on the homepage. The videos are populated with the "conference video" content type.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('videos-widget')),
);

$form['conference_homepage']['conference_videos']['conference_videos_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_videos_title', ''),
  '#description'    => t('Title to display above the videos.'),
);

$conference_videos_intro = variable_get('conference_videos_intro', '');
if (is_array($conference_videos_intro)) {
  $conference_videos_intro = $conference_videos_intro['value'];
}

$term   = variable_get('conference_videos_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_videos']['conference_videos_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_videos_intro,
  '#description'    => t('Introductory text to display above the videos.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-videos-intro-format { display: none; }</style>',
);

$form['conference_homepage']['conference_videos']['conference_videos_button_label'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Button label'),
  '#default_value'  => variable_get('conference_videos_button_label', ''),
  '#description'    => t('Optionally enter a button label. This will create a button with this label that links to the videos page.'),
);
