<?php
/**
 * @file
 * Code for content block widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_content_block'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Content block widget'),
  '#description'    => t('Display a block of arbitrary content. This is useful for writing introductory text for your conference.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('content-block-widget')),
);

$form['conference_homepage']['conference_content_block']['conference_content_block_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_content_block_title', ''),
  '#description'    => t('Optionally enter a title. The title appears above the body.'),
);

$conference_content_block_body = variable_get('conference_content_block_body', '');
if (is_array($conference_content_block_body)) {
  $conference_content_block_body = $conference_content_block_body['value'];
}

$term   = variable_get('conference_content_block');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_content_block']['conference_content_block_body'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Body'),
  '#default_value'  => $conference_content_block_body,
  '#description'    => t('Enter the body.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-content-block-body-format { display: none; }</style>',
);
