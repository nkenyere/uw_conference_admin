<?php
/**
 * @file
 * Code for mailchimp-related fields in UW Conference Admin form.
 */

$form['conference']['conference_contact_information']['conference_mailchimp'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('MailChimp settings'),
  '#description'    => t('Settings for MailChimp newsletter subscriptions'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_mailchimp_title', ''),
  '#description'    => t('Title of the form.'),
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_submit_text'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Submit button text'),
  '#default_value'  => variable_get('conference_mailchimp_submit_text', ''),
  '#description'    => t('Label text on the submit button. E.g. "Submit".'),
  '#maxlength'      => 12,
);

$conference_mailchimp_description = variable_get('conference_mailchimp_description', '');
if (is_array($conference_mailchimp_description)) {
  $conference_mailchimp_description = $conference_mailchimp_description['value'];
}

$term   = variable_get('conference_mailchimp_description');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_description'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Description'),
  '#default_value'  => $conference_mailchimp_description,
  '#description'    => t('Description of the form.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-mailchimp-description-format { display: none; }</style>',
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_action'] = array(
  '#type'           => 'textfield',
  '#title'          => t('MailChimp action'),
  '#default_value'  => variable_get('conference_mailchimp_action', ''),
  '#description'    => t('Form action. This should be a URL.'),
  '#element_validate' => array('_uw_conference_admin_url_validation'),
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_user'] = array(
  '#type'           => 'textfield',
  '#title'          => t('User'),
  '#default_value'  => variable_get('conference_mailchimp_user', ''),
  '#description'    => t('User/account ID.'),
);

$form['conference']['conference_contact_information']['conference_mailchimp']['conference_mailchimp_list'] = array(
  '#type'           => 'textfield',
  '#title'          => t('List'),
  '#default_value'  => variable_get('conference_mailchimp_list', ''),
  '#description'    => t('List ID.'),
);
