<?php
/**
 * @file
 * Code for theme-related fields in UW Conference Admin form.
 */

$form['conference']['conference_theme'] = array(
  '#type'           => 'fieldset',
  '#title'          => 'Theme settings',
  '#description'    => t('Customize the conference theme. Choose a default banner and create your custom site footer.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
);

$form['conference']['conference_theme']['conference_banner'] = array(
  '#type'            => 'managed_file',
  '#title'           => t('Banner image'),
  '#default_value'   => variable_get('conference_banner', ''),
  '#description'     => t('Upload a default banner image. You can override it on a page by page basis. Minimum size 1300x768. Maximum size 3600x1536.'),
  '#upload_location' => 'public://',
  '#upload_validators' => array(
    'file_validate_is_image'         => array(),
    'file_validate_extensions'       => array('gif png jpg jpeg svg'),
    'file_validate_size'             => array(5 * 1024 * 1024),
    'file_validate_image_resolution' => array('3600x1536', '1800x768'),
  ),
);

$banner_fid = variable_get('conference_banner');
if ($banner_fid) {
  $banner_file = file_load($banner_fid);
  if ($banner_file) {
    $banner_img  = '<img src="' . file_create_url($banner_file->uri) . '" alt="Default Banner Image" style="border: 1px solid #ccc; max-width: 50%; padding: 5px;" />';
  }
  else {
    $banner_img = '';
  }
}
else {
  $banner_img  = '';
}

$form['conference']['conference_theme']['conference_banner_preview'] = array(
  '#prefix' => '<p>',
  '#suffix' => $banner_img . '</p>',
);

$conference_theme_custom_footer = variable_get('conference_theme_custom_footer', '');
if (is_array($conference_theme_custom_footer)) {
  $conference_theme_custom_footer = $conference_theme_custom_footer['value'];
}

$term   = variable_get('conference_theme_custom_footer');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference']['conference_theme']['conference_theme_custom_footer'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Custom footer'),
  '#default_value'  => $conference_theme_custom_footer,
  '#description'    => t('Custom footer. You may enter HTML.'),
  '#required'       => FALSE,
  '#format'         =>  $format,
);
