<?php
/**
 * @file
 * Code for link widget fields in UW Conference Admin form.
 */

$form['conference_homepage']['conference_links'] = array(
  '#type'           => 'fieldset',
  '#title'          => t('Links widget'),
  '#description'    => t('Display a list of links. The links are populated from menu items in the "conference links" menu.'),
  '#collapsible'    => TRUE,
  '#collapsed'      => FALSE,
  '#attributes'     => array('id' => array('links-widget')),
);

$form['conference_homepage']['conference_links']['conference_links_title'] = array(
  '#type'           => 'textfield',
  '#title'          => t('Title'),
  '#default_value'  => variable_get('conference_links_title', ''),
  '#description'    => t('Optionally enter a title. It appears at the top of the widget.'),
);

$conference_links_intro = variable_get('conference_links_intro', '');
if (is_array($conference_links_intro)) {
  $conference_links_intro = $conference_links_intro['value'];
}

$term   = variable_get('conference_links_intro');
$format = ($term && is_array($term) && isset($term['format'])) ? $term['format'] : 'uw_tf_basic';
$format = ($format === 'plain_text') ? 'uw_tf_basic' : $format;

$form['conference_homepage']['conference_links']['conference_links_intro'] = array(
  '#type'           => 'text_format',
  '#title'          => t('Introduction'),
  '#default_value'  => $conference_links_intro,
  '#description'    => t('Optionally enter an introduction. It appears at the top of the widget, underneath the title.'),
  '#format'         => $format,
  'format'          => $format,
  '#suffix'         => '<style>#edit-conference-links-intro-format { display: none; }</style>',
);
